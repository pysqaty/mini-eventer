package com.namenotfoundexception.hackatonproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.indoorway.android.common.sdk.listeners.generic.Action0;
import com.indoorway.android.common.sdk.listeners.generic.Action1;
import com.indoorway.android.common.sdk.model.Coordinates;
import com.indoorway.android.common.sdk.model.IndoorwayMap;
import com.indoorway.android.common.sdk.model.IndoorwayObjectParameters;
import com.indoorway.android.common.sdk.model.IndoorwayPosition;
import com.indoorway.android.location.sdk.IndoorwayLocationSdk;
import com.indoorway.android.map.sdk.listeners.OnObjectSelectedListener;
import com.indoorway.android.map.sdk.view.IndoorwayMapView;
import com.indoorway.android.map.sdk.view.drawable.figures.DrawableIcon;
import com.indoorway.android.map.sdk.view.drawable.layers.MarkersLayer;
import com.indoorway.android.map.sdk.view.drawable.textures.BitmapTexture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    String ipAddress;

    LinearLayout ourLayout;
    IndoorwayMapView indoorwayMapView;
    IndoorwayMap indoorwayMap;
    MarkersLayer clickableLayer;

    EventsRepository eventsRepository;

    String[] floorUuids;
    String buildingUuid;

    HashMap<String, Integer> floors;

    HashMap<Event.Type, String> type_icons_ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        readResources();

        indoorwayMapView = findViewById(R.id.mapView);
        setUpMap();
    }

    // resources reading // ------------------------------------------------------------------------

    private void readResources() {
        buildingUuid = getResources().getString(R.string.building_uuid);
        ourLayout = findViewById(R.id.ourLayout);
        String floor0Uuid = getResources().getString(R.string.floor_0);
        String floor1Uuid = getResources().getString(R.string.floor_1);
        String floor2Uuid = getResources().getString(R.string.floor_2);

        type_icons_ids = new HashMap<>();
        type_icons_ids.put(Event.Type.GAMES, "icon_games");
        type_icons_ids.put(Event.Type.LEARNING, "icon_learning");
        type_icons_ids.put(Event.Type.OTHER, "icon_other");

        floors = new HashMap<>();
        floorUuids = new String[]{floor0Uuid, floor1Uuid, floor2Uuid};
        floors.put(floorUuids[0], 0);
        floors.put(floorUuids[1], 1);
        floors.put(floorUuids[2], 2);
        eventsRepository = HackatonApplication.eventsRepository;
        readServerIP();
    }

    @Override
    public void onBackPressed() {
        if (HackatonApplication.isTracing) {
            indoorwayMapView.getNavigation().stop();
            HackatonApplication.isTracing = false;
            HackatonApplication.selectedEvent = null;
        } else {
            super.onBackPressed();
        }
    }

    private void setUpMap() {
        // ----------------------------- OnClickListener -------------------------------------------
        final MainActivity act = this;
        indoorwayMapView.getTouch().setOnClickListener(new Action1<Coordinates>() {
            @Override
            public void onAction(Coordinates coordinates) {
                if (clickableLayer != null) {
                    for (String s : clickableLayer.getFiguresAt(coordinates)) {
                        new EventLayoutView(act, eventsRepository.getEventById(s, HackatonApplication.floorNo));
                    }
                    HackatonApplication.touchedCoordinates = coordinates;
                }
            }
        });

        // ----------------------------- OnMoveListener --------------------------------------------
        indoorwayMapView.getTouch().setOnMoveListener(new Action0() {
            @Override
            public void onAction() {
                HackatonApplication.isFollowing = false;
            }
        });

        // ---------------------------- IMPORTANT --------------------------------------------------
        // --------------------- OnLoadCompletedListener -------------------------------------------
        indoorwayMapView.setOnMapLoadCompletedListener(new Action1<IndoorwayMap>() {
            @Override
            public void onAction(IndoorwayMap map) {
                clickableLayer = indoorwayMapView.getMarker().addLayer(3.0f);
                indoorwayMap = map;
                Bitmap bitmap0 = BitmapFactory.decodeResource(getResources(), R.drawable.icon1);
                clickableLayer.registerTexture(new BitmapTexture(type_icons_ids.get(Event.Type.GAMES), bitmap0));
                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.icon2);
                clickableLayer.registerTexture(new BitmapTexture(type_icons_ids.get(Event.Type.LEARNING), bitmap1));
                Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.icon3);
                clickableLayer.registerTexture(new BitmapTexture(type_icons_ids.get(Event.Type.OTHER), bitmap2));

                drawEvents();
            }
        });

        // --------------------- OnMapLoadFailedListener -------------------------------------------
        indoorwayMapView.setOnMapLoadFailedListener(new Action0() {
            @Override
            public void onAction() {
                Log.v("xd", "XD");
            }
        });

        // ---------------------------- IMPORTANT --------------------------------------------------
        // --------------------- OnObjectSelectedListener ------------------------------------------
        indoorwayMapView.getSelection().setOnObjectSelectedListener(new OnObjectSelectedListener() {
            @Override
            public boolean canObjectBeSelected(IndoorwayObjectParameters indoorwayObjectParameters) {
                return Arrays.asList("elevator", "stairs")
                        .contains(indoorwayObjectParameters.getType());
            }

            @Override
            public void onObjectSelected(IndoorwayObjectParameters indoorwayObjectParameters) {
                ourLayout.setVisibility(View.VISIBLE);
                indoorwayMapView.getCamera().setPosition(indoorwayObjectParameters.getCenterPoint());
            }

            @Override
            public void onSelectionCleared() {
                ourLayout.setVisibility(View.INVISIBLE);
            }
        });

        // --------------------- PositionListener --------------------------------------------------
        Action1<IndoorwayPosition> positionListener = new Action1<IndoorwayPosition>() {
            @Override
            public void onAction(IndoorwayPosition position) {
                Log.i("trace", "XD");
                indoorwayMapView.getPosition().setPosition(position, HackatonApplication.isFollowing);
                if (HackatonApplication.isFollowing) {
                    indoorwayMapView.getCamera().setPosition(position.getCoordinates());
                }
                HackatonApplication.actualPosition = position;
                HackatonApplication.floorNo = floors.get(indoorwayMap.getMapUuid());

                if (HackatonApplication.isTracing) {
                    if (HackatonApplication.selectedEvent == null || HackatonApplication.actualPosition.getCoordinates().getDistanceTo(HackatonApplication.selectedEvent.coords) < 0.01 || HackatonApplication.selectedEvent.coords.getDistanceTo(HackatonApplication.actualPosition.getCoordinates()) < 0.01) {
                        HackatonApplication.isTracing = false;
                        indoorwayMapView.getNavigation().stop();
                    } else {
                        routeClick();
                    }
                }
            }
        };

        IndoorwayLocationSdk.instance().position().onChange().register(positionListener);
        indoorwayMapView.load(buildingUuid, floorUuids[2]);
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                fetchEvents();
            }
        }, 0, 10000);
    }

    // --------------------- ChangeFloorListener ---------------------------------------------------
    public void buttonClick(View view) {
        String buildingUuid = getResources().getString(R.string.building_uuid);
        String newMapUUID = "";
        switch (view.getId()) {
            case R.id.buttonFloor2:
                newMapUUID = floorUuids[2];
                break;
            case R.id.buttonFloor1:
                newMapUUID = floorUuids[1];
                break;
            case R.id.buttonFloor0:
                newMapUUID = floorUuids[0];
                break;
        }
        indoorwayMapView.load(buildingUuid, newMapUUID);
        HackatonApplication.floorNo = floors.get(newMapUUID);
        ourLayout.setVisibility(View.INVISIBLE);
    }

    public void followClick(View view) {
        HackatonApplication.isFollowing = true;
        indoorwayMapView.getCamera().setPosition(HackatonApplication.actualPosition.getCoordinates());
    }

    public void routeClick() {
        Event event = HackatonApplication.selectedEvent;
        int visitorFloor = floors.get(HackatonApplication.actualPosition.getMapUuid());
        if (HackatonApplication.isFollowing) {
            indoorwayMapView.load(buildingUuid, floorUuids[visitorFloor]);
            HackatonApplication.floorNo = visitorFloor;

            if (event.floorNo == visitorFloor) {
                indoorwayMapView.getNavigation()
                        .start(HackatonApplication.actualPosition, event.coords);
            } else {
                Coordinates stairsCoordinates = indoorwayMap.objectsWithType("stairs").get(0).getCenterPoint();
                indoorwayMapView.getNavigation()
                        .start(HackatonApplication.actualPosition, stairsCoordinates);
            }
        } else {
            if (HackatonApplication.floorNo == event.floorNo) {
                if (floors.get(HackatonApplication.actualPosition.getMapUuid()) == HackatonApplication.floorNo) {
                    indoorwayMapView.getNavigation()
                            .start(HackatonApplication.actualPosition.getCoordinates(), event.coords);
                } else {
                    Coordinates stairsCoordinates = indoorwayMap.objectsWithType("stairs").get(0).getCenterPoint();
                    indoorwayMapView.getNavigation()
                            .start(stairsCoordinates, event.coords);
                }
            } else {
                if (floors.get(HackatonApplication.actualPosition.getMapUuid()) == HackatonApplication.floorNo) {
                    Coordinates stairsCoordinates = indoorwayMap.objectsWithType("stairs").get(0).getCenterPoint();
                    indoorwayMapView.getNavigation()
                            .start(HackatonApplication.actualPosition, stairsCoordinates);
                } else {
                    indoorwayMapView.getNavigation().stop();
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.allEvents:
                Intent intent = new Intent(this, EventsActivity.class);
                startActivityForResult(intent, 1);
                return true;
            case R.id.newEventCreate:
                final MainActivity act = this;
                Event event = new Event();
                event.coords = HackatonApplication.actualPosition.getCoordinates();
                event.floorNo = HackatonApplication.floorNo;
                new NewEventLayoutView(act, event);
                return true;
            case R.id.setIp:
                setIpLayout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void fetchEvents() {
        Log.i("json", "fetch");
        JsonObjectRequest request =
                new JsonObjectRequest(Request.Method.GET, ipAddress + "/events", null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                ArrayList<Event> eventsList = Event.parseFromJSON(response);
                                clearEvents();
                                for (Event e : eventsList) {
                                    eventsRepository.addEvent(e, e.floorNo);
                                    Log.i("json", e.title);
                                }
                                drawEvents();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i("json", error + "");
                            }
                        });
        HackatonApplication.requestQueue.add(request);
    }

    public void drawEvents() {
        if (indoorwayMap == null) {
            return;
        }
        for (HashMap.Entry<String, Event> entry : eventsRepository.getEvents(HackatonApplication.floorNo)) {
            if (entry.getValue().floorNo == floors.get(indoorwayMap.getMapUuid())) {
                clickableLayer.add(
                        new DrawableIcon(
                                entry.getKey(),
                                type_icons_ids.get(entry.getValue().type),
                                entry.getValue().coords,
                                2f, 2f
                        )
                );
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            int floorNo = data.getIntExtra("floorNo", 0);
            double crdX = data.getDoubleExtra("crdX", 0);
            double crdY = data.getDoubleExtra("crdY", 0);
            String eventId = data.getStringExtra("eventId");

            if (HackatonApplication.floorNo != floorNo) {
                indoorwayMapView.load(buildingUuid, floorUuids[floorNo]);
                HackatonApplication.floorNo = floorNo;
            }
            indoorwayMapView.getCamera().setPosition(new Coordinates(crdX, crdY));

            if (!eventsRepository.getEventsIds().contains(eventId)) {
                Toast toast = Toast.makeText(getApplicationContext(), "Event no longer exists", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    private void clearEvents() {
        if (clickableLayer != null) {
            for (String eventId : eventsRepository.getEventsIds()) {
                clickableLayer.remove(eventId);
            }
            eventsRepository.Clear();
        }
    }

    public void readEventsFromServer(Event event) {
        JSONObject eventObj = new JSONObject();
        try {
            eventObj.put("title", event.title);
            eventObj.put("description", event.description);
            eventObj.put("floorNo", event.floorNo);
            eventObj.put("type", event.type.ordinal());
            eventObj.put("time", event.timeInSeconds);
            JSONArray coords = new JSONArray(new double[]{event.coords.component1(), event.coords.component2()});
            eventObj.put("coordinates", coords);
        } catch (JSONException e) {
            Log.e("json", "err parsing JSON");
        }

        Log.i("json", eventObj.toString());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, ipAddress + "/add-event", eventObj, null, null);
        HackatonApplication.requestQueue.add(request);
    }

    public void setIpLayout() {
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.set_ip_layout, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(convertView);
        final Dialog dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        final TextView ipText = convertView.findViewById(R.id.ipText);
        ipText.setText(ipAddress);
        Button submitButton = convertView.findViewById(R.id.ipSubmit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setServerIP(ipText.getText().toString());
                dialog.cancel();
            }
        });

    }

    private void readServerIP(){
        SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.settings_file), 0);
        ipAddress = "http://"+settings.getString("ip_address", getResources().getString(R.string.default_ip))+":"+getResources().getString(R.string.server_port);
    }

    private void setServerIP(String ipString){
        SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.settings_file), 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ip_address", ipString);
        editor.apply();
    }
}
