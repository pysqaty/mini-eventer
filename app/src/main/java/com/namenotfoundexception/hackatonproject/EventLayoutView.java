package com.namenotfoundexception.hackatonproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class EventLayoutView {

    public EventLayoutView(final MainActivity activity, final Event event) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View convertView = inflater.inflate(R.layout.event_layout, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(convertView);
        final Dialog dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        TextView tittle = convertView.findViewById(R.id.title);
        TextView description = convertView.findViewById(R.id.description);
        Button followButton = convertView.findViewById(R.id.routeButton);

        tittle.setText(event.title);
        description.setText(event.description);
        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HackatonApplication.selectedEvent = event;
                HackatonApplication.isTracing = true;
                activity.routeClick();
                dialog.cancel();
            }
        });

    }
}
