package com.namenotfoundexception.hackatonproject;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class EventListViewAdapter extends BaseExpandableListAdapter {

    private List<Event> events;
    private LayoutInflater inflater;
    private Activity activity;

    public EventListViewAdapter(Activity activity, List<Event> events) {
        this.events = events;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public int getGroupCount() {
        return events.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return events.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return events.get(i);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.event_name, null);
        }
        TextView textView = view.findViewById(R.id.eventName);
        textView.setText(events.get(i).title);
        return view;
    }

    @Override
    public View getChildView(final int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.event_desc, null);
        }
        TextView textView = view.findViewById(R.id.eventDescription);
        textView.setText(events.get(i).description + '\n' + "Floor: " + events.get(i).floorNo);
        Button button = view.findViewById(R.id.eventFind);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = activity.getIntent();
                intent.putExtra("crdX", events.get(i).coords.component1());
                intent.putExtra("crdY", events.get(i).coords.component2());
                intent.putExtra("floorNo", events.get(i).floorNo);
                intent.putExtra("eventId", events.get(i).hashCode() + "");
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
            }
        });
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

}
