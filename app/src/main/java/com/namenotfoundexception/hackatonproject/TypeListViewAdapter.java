package com.namenotfoundexception.hackatonproject;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TypeListViewAdapter extends BaseExpandableListAdapter {

    private LayoutInflater inflater;
    private Event.Type currentType;

    public TypeListViewAdapter(Activity activity) {
        inflater = activity.getLayoutInflater();
        currentType = Event.Type.values()[0];
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public int getChildrenCount(int i) {
        return Event.Type.values().length;
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return Event.Type.values()[i1].name();
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.event_type_layout, null);
        }
        TextView textView = view.findViewById(R.id.typeText);
        String str = currentType.name();
        textView.setText(str);
        ImageView imageView = view.findViewById(R.id.typeImage);
        imageView.setImageResource(currentType.drawId);

        return view;

    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.event_type_layout, null);
        }
        TextView textView = view.findViewById(R.id.typeText);
        textView.setText(Event.Type.values()[i1].name());
        ImageView imageView = view.findViewById(R.id.typeImage);
        imageView.setImageResource(Event.Type.values()[i1].drawId);
        ((TextView) view.findViewById(R.id.selectTypeInfo)).setText("");
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    public void changeType(int i) {
        currentType = Event.Type.values()[i];
        notifyDataSetChanged();
    }

    public Event.Type getType() {
        return currentType;
    }
}
