package com.namenotfoundexception.hackatonproject;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.indoorway.android.common.sdk.IndoorwaySdk;
import com.indoorway.android.common.sdk.model.Coordinates;
import com.indoorway.android.common.sdk.model.IndoorwayPosition;

public class HackatonApplication extends Application {

    public static RequestQueue requestQueue;
    public static boolean isFollowing = true;
    public static IndoorwayPosition actualPosition;
    public static int floorNo;
    public static EventsRepository eventsRepository;
    public static Coordinates touchedCoordinates;
    public static boolean isTracing = false;
    public static Event selectedEvent;

    @Override
    public void onCreate() {
        super.onCreate();

        IndoorwaySdk.initContext(this);
        requestQueue = Volley.newRequestQueue(this);
        String teamApiKey = getResources().getString(R.string.team_API_key);
        IndoorwaySdk.configure(teamApiKey);
        eventsRepository = new EventsRepository();
    }
}
