package com.namenotfoundexception.hackatonproject;

import android.util.Log;

import com.indoorway.android.common.sdk.model.Coordinates;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Event {
    public enum Type {
        GAMES(R.drawable.icon1),
        LEARNING(R.drawable.icon2),
        OTHER(R.drawable.icon3);

        public int drawId;

        Type(int drawId) {
            this.drawId = drawId;
        }
    }

    public String title;
    public String description;
    public Coordinates coords;
    public int floorNo;
    public Type type;
    public int timeInSeconds;

    public Event() {
    }

    public Event(String tittle, String description, Coordinates coordinates, int floorNo, Type type) {
        this.title = tittle;
        this.description = description;
        this.coords = coordinates;
        this.floorNo = floorNo;
        this.type = type;
    }

    public static ArrayList<Event> parseFromJSON(JSONObject jsonObject) {
        ArrayList<Event> events = new ArrayList<>();
        try {

            JSONArray eventsArray = jsonObject.getJSONArray("events");

            for (int i = 0; i < eventsArray.length(); i++) {
                JSONObject event = eventsArray.getJSONObject(i);
                String title = event.getString("title");
                String description = event.getString("description");
                JSONArray coordsArray = event.getJSONArray("coordinates");
                Coordinates coords = new Coordinates(coordsArray.getDouble(0), coordsArray.getDouble(1));
                int floorNo = event.getInt("floorNo");
                Type type = Type.values()[event.getInt("type")];
                events.add(new Event(title, description, coords, floorNo, type));
            }
        } catch (JSONException e) {
            Log.e("JSON", "JSON parser error");
        }
        return events;
    }
}
