package com.namenotfoundexception.hackatonproject;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

public class EventsActivity extends AppCompatActivity {
    private EventsRepository eventsRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        ExpandableListView listView = findViewById(R.id.eventsList);
        eventsRepository = HackatonApplication.eventsRepository;
        listView.setAdapter(new EventListViewAdapter(this, eventsRepository.getEventsAsList()));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED, new Intent());
    }
}
