package com.namenotfoundexception.hackatonproject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class EventsRepository {
    private ConcurrentHashMap<String, Event>[] events;

    @SuppressWarnings("unchecked")
    public EventsRepository() {
        events = new ConcurrentHashMap[3];
        for (int i = 0; i < 3; i++) {
            events[i] = new ConcurrentHashMap<>();
        }
    }

    public void addEvent(Event e, int floorNo) {
        events[floorNo].put(e.hashCode() + "", e);
    }

    public Event getEventById(String eventId, int floorNo) {
        return events[floorNo].get(eventId);
    }

    public Set<Map.Entry<String, Event>> getEvents(int floorNo) {
        return events[floorNo].entrySet();
    }

    public List<Event> getEventsAsList() {
        List<Event> eventsList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            eventsList.addAll(this.events[i].values());
        }
        return eventsList;
    }

    public List<String> getEventsIds() {
        List<String> listOfEventsIds = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            listOfEventsIds.addAll(this.events[i].keySet());
        }
        return listOfEventsIds;
    }

    public void Clear() {
        for (ConcurrentHashMap<String, Event> eventsOnFloor : events) {
            eventsOnFloor.clear();
        }
    }
}
