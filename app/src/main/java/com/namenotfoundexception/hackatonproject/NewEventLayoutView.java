package com.namenotfoundexception.hackatonproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;

public class NewEventLayoutView {
    private EditText tittle;
    private EditText description;
    private Button confirmButton;
    private EditText minute;
    private EditText hour;

    public NewEventLayoutView(final MainActivity activity, final Event event) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View convertView = inflater.inflate(R.layout.new_event_def, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(convertView);
        final Dialog dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        //convertView = convertView.findViewById(R.id.scrollView);
        tittle = convertView.findViewById(R.id.newTitle);
        description = convertView.findViewById(R.id.newDesc);
        confirmButton = convertView.findViewById(R.id.confirmButton);
        hour = convertView.findViewById(R.id.newHour);
        minute = convertView.findViewById(R.id.newMinute);

        hour.setFilters(new InputFilter[]{new MinMaxFilter("0", "24")});
        minute.setFilters(new InputFilter[]{new MinMaxFilter("0", "59")});


        final ExpandableListView listView = convertView.findViewById(R.id.typesList);
        final TypeListViewAdapter adapter = new TypeListViewAdapter(activity);
        listView.setAdapter(adapter);
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                adapter.changeType(i1);
                listView.collapseGroup(0);
                return false;
            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.description = description.getText().toString();
                event.title = tittle.getText().toString();
                event.type = adapter.getType();
                event.timeInSeconds = Integer.parseInt(hour.getText().toString()) * 60 * 60 + Integer.parseInt(minute.getText().toString()) * 60;
                activity.readEventsFromServer(event);
                dialog.cancel();
            }
        });
    }

    public class MinMaxFilter implements InputFilter {

        private int mIntMin, mIntMax;

        public MinMaxFilter(int minValue, int maxValue) {
            this.mIntMin = minValue;
            this.mIntMax = maxValue;
        }

        public MinMaxFilter(String minValue, String maxValue) {
            this.mIntMin = Integer.parseInt(minValue);
            this.mIntMax = Integer.parseInt(maxValue);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(mIntMin, mIntMax, input))
                    return null;
            } catch (NumberFormatException nfe) {
            }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }
}
