import atexit
from flask import *
from apscheduler.scheduler import Scheduler
import json
app = Flask(__name__)

event_data = {'events':[]}

@app.route("/events")
def events():
    data = json.dumps(event_data)
    return data

@app.route("/add-event", methods=['POST'])
def add_event():
    event_data['events'] += [request.json]
    return "OK"

def decrease_time():
    print event_data
    out_arr = []
    for event in event_data['events']:
        event['time'] -= 60
        if event['time'] > 0:
            out_arr += [event]
    event_data['events'] = out_arr

def save():
    with open('events.json', 'w') as f:
        f.write(json.dumps(event_data))

@app.before_first_request
def init():
    global event_data
    event_data = json.load(open('events.json'))
    scheduler = Scheduler()
    scheduler.start()

    scheduler.add_interval_job(decrease_time, seconds=1)

atexit.register(save)

app.run(host='0.0.0.0')
